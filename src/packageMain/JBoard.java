package packageMain;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JPanel;


public class JBoard extends JPanel implements MouseMotionListener{
    
	private static boolean init = false;
	
	public static boolean StopPan = false;
	
    private MouseProprieties zoomAndPanListener;
    private static Graphics2D g ;
    private static ArrayList<ShapeProprieties> array_objet = new ArrayList<ShapeProprieties>();
    private static boolean axeVisible = false;
    private static double MaximumDeplacementx1 = -5000;
    private static double MaximumDeplacementy1 = 5000;
    private static double MaximumDeplacementx2 = 5000;
    private static double MaximumDeplacementy2 = -5000;
    
    
    public JBoard() {
    	this.zoomAndPanListener = new MouseProprieties(this);
    	zoomAndPanListener.setTargetComp(this);
    	System.out.println(this.size());
        this.addMouseListener(zoomAndPanListener);
        this.addMouseMotionListener(zoomAndPanListener);
        this.addMouseWheelListener(zoomAndPanListener);    
        zoomAndPanListener.setMaxZoomLevel(30);
        this.addMouseMotionListener(this);
        
        setMaximumDeplacementCoordonate(MaximumDeplacementx1, MaximumDeplacementy1, MaximumDeplacementx2, MaximumDeplacementy2 , true);
        
    }



    public void paint(Graphics g1) {
    	super.paintComponent(g1);
	    	
	    	g = (Graphics2D) g1;
	    	
	    	if(init == false){
	        	
	        	init = true;
	            Dimension d = getSize();
	            int xc = d.width / 1;
	            int yc = d.height / 1;
	            g.translate(xc, yc);
	            g.scale(1, -1);
	            // Save the viewport to be updated by the ZoomAndPanListener
	            zoomAndPanListener.setCoordTransform(g.getTransform());
	            
	        }
	        g.setTransform(zoomAndPanListener.getCoordTransform());
	       
	        /*
	         * Ins�rer des formes ici
	         * 
	         * ex:
	         * g.fillRect();
	         * 
	         */
	        
	        
	        for(int i = 0; i<array_objet.size() ; i++){
	        	g.setColor(array_objet.get(i).g.getColor());
	        	
	        	if(array_objet.get(i).filled == true){
	        		g.fill(array_objet.get(i).shape);
	        	}else{
	        		g.draw(array_objet.get(i).shape);
	        	}
	        	g.setColor(Color.BLACK);
	        }
	        
	        if(axeVisible == true){
		         int maxval = 100000;
		         g.setStroke(new BasicStroke(10));
		         g.drawLine(0, -maxval, 0, maxval);
		         
		         g.drawLine(-maxval, 0, maxval, 0);
	        }
    }
    
    public ShapeProprieties AddShape(Shape shape, Graphics g , boolean filled){
    	
    	ShapeProprieties newShapeProp = new ShapeProprieties();
    	newShapeProp.g = g;
    	newShapeProp.shape = shape;
    	newShapeProp.filled = filled;
    	array_objet.add(newShapeProp);
    	repaint();
    	return newShapeProp;
    }
    public void removeShape(Shape prop){
    	for (int i = 0; i < array_objet.size(); i++) {
	    	if(array_objet.get(i).shape == prop){
	    		array_objet.remove(i);
	    	}    	
    	}
    	repaint();
    }
    
    void setAxeVisible(boolean visible){
    	axeVisible = visible;
    }
    public void translate(double x , double y){
    	zoomAndPanListener.translate(x, y);
    	repaint();
    }

    public void setMaximumDeplacementCoordonate(double x1 , double y1 , double x2 , double y2 , boolean centered){
    	MaximumDeplacementx1 = x1;
        MaximumDeplacementy1 = y1;
        MaximumDeplacementx2 = x2;
        MaximumDeplacementy2 = y2;
    	zoomAndPanListener.setMaximumDeplacementCoordonate(MaximumDeplacementx1 , MaximumDeplacementy1 , MaximumDeplacementx2 , MaximumDeplacementy2 , centered);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}}