package packageMain;

import java.awt.BasicStroke;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.border.Border;
import javax.swing.border.StrokeBorder;

public class Main{
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame f = new JFrame();
		f.setVisible(true);
		f.setLayout(new CardLayout());
		f.setSize(500, 500);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JBoard jb = new JBoard();
		jb.setSize(500, 500);
		f.add(jb);
		jb.setAxeVisible(true);
		jb.setMaximumDeplacementCoordonate(-1000, 1000, 1000, -1000 , true);
		jb.translate(f.getContentPane().getWidth()/2, f.getContentPane().getHeight()/2);
		
		
		Graphics2D graphics = (Graphics2D)f.getGraphics();
		graphics.setColor(Color.red);
		jb.AddShape(new Rectangle(900,-1000,100,100), graphics, true);
		
		
		
	}
	public static Rectangle newLine(int x , int y , int w , int h){
		Rectangle rect = new Rectangle();
		rect.setBounds(x, y, w, h);
		return rect;
	}
		
}
