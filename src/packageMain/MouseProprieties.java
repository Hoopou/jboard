package packageMain;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MouseProprieties implements MouseListener, MouseMotionListener, MouseWheelListener {
    public static final int DEFAULT_MIN_ZOOM_LEVEL = 20;
    public static final int DEFAULT_MAX_ZOOM_LEVEL = 30;
    public static final double DEFAULT_ZOOM_MULTIPLICATION_FACTOR = 1.2;

    private Component targetComponent;

    private int zoomLevel = 20;
    private int minZoomLevel = DEFAULT_MIN_ZOOM_LEVEL;
    private int maxZoomLevel = DEFAULT_MAX_ZOOM_LEVEL;
    private double zoomMultiplicationFactor = DEFAULT_ZOOM_MULTIPLICATION_FACTOR;
    
    private Point dragStartScreen;
    private Point dragEndScreen;
    private AffineTransform coordTransform = new AffineTransform();
    
    private static double MaximumDeplacementx1 = 3000;
    private static double MaximumDeplacementy1 = 3000;
    private static double MaximumDeplacementx2 = -3000;
    private static double MaximumDeplacementy2 = -3000;
    

    public MouseProprieties(Component targetComponent) {
        this.targetComponent = targetComponent;
        coordTransform.translate(0, 0);
    }


    public void mouseClicked(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    	dragStartScreen = e.getPoint();
        dragEndScreen = null;
    }

    public void mouseReleased(MouseEvent e) {
//        moveCamera(e);
    	System.out.println((int)getCoordTransform().getTranslateX() + " : " + (int)getCoordTransform().getTranslateY() + " : " + zoomLevel);
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {     
    	System.out.println(coordTransform.getTranslateX()/(1.0/zoomLevel) + " - " + coordTransform.getTranslateY() + " : " + zoomLevel);
	    if(coordTransform.getTranslateX() <= MaximumDeplacementx1 &&
	    		coordTransform.getTranslateX() >= MaximumDeplacementx2 &&
	    		coordTransform.getTranslateY() <= MaximumDeplacementy1 &&
	    		coordTransform.getTranslateY() >= MaximumDeplacementy2){
//			zoomAndPanListener.translate(MaximumDeplacementx1-this.getWidth()/2, zoomAndPanListener.getCoordTransform().getTranslateY());
	    	moveCamera(e);
	    	
		}
	    if(coordTransform.getTranslateX() >= MaximumDeplacementx1){
			translate(MaximumDeplacementx1-coordTransform.getTranslateX()-2, 0);
		}else if(coordTransform.getTranslateX() <= MaximumDeplacementx2){
			translate(MaximumDeplacementx2-coordTransform.getTranslateX()+2, 0);
		}else if(coordTransform.getTranslateY() >= MaximumDeplacementy1){
			translate(0, coordTransform.getTranslateY()-MaximumDeplacementy1+2);
		}else if(coordTransform.getTranslateY() <= MaximumDeplacementy2){
			translate(0, coordTransform.getTranslateY()-MaximumDeplacementy2-2);
		}
//		System.out.println(zoomAndPanListener.getCoordTransform().getTranslateX() + " - " + zoomAndPanListener.getCoordTransform().getTranslateY() + " -" + StopPan);
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
//        System.out.println("============= Zoom camera ============");
        zoomCamera(e);
    }

//Fin des listeners
    
    
    
    private void moveCamera(MouseEvent e) {
//        System.out.println("============= Move camera ============");
        try {
            dragEndScreen = e.getPoint();
            Point2D.Float dragStart = transformPoint(dragStartScreen);
            Point2D.Float dragEnd = transformPoint(dragEndScreen);
            double dx = dragEnd.getX() - dragStart.getX();
            double dy = dragEnd.getY() - dragStart.getY();
            coordTransform.translate(dx, dy);
            dragStartScreen = dragEndScreen;
            dragEndScreen = null;
            targetComponent.repaint();
        } catch (NoninvertibleTransformException ex) {
            ex.printStackTrace();
        }
    }

    private void zoomCamera(MouseWheelEvent e) {
        try {
            int wheelRotation = e.getWheelRotation();
            Point p = e.getPoint();
            if (wheelRotation > 0) {
                if (zoomLevel < maxZoomLevel) {
                    zoomLevel++;
                    Point2D p1 = transformPoint(p);
                    coordTransform.scale(1 / zoomMultiplicationFactor, 1 / zoomMultiplicationFactor);
                    Point2D p2 = transformPoint(p);
                    coordTransform.translate(p2.getX() - p1.getX(), p2.getY() - p1.getY());
                    targetComponent.repaint();
                }
            } else {
                if (zoomLevel > minZoomLevel) {
                    zoomLevel--;
                    Point2D p1 = transformPoint(p);
                    coordTransform.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
                    Point2D p2 = transformPoint(p);
                    coordTransform.translate(p2.getX() - p1.getX(), p2.getY() - p1.getY());
                    targetComponent.repaint();
                }
            }
        } catch (NoninvertibleTransformException ex) {
            ex.printStackTrace();
        }
    }

    private Point2D.Float transformPoint(Point p1) throws NoninvertibleTransformException {
//        System.out.println("Model -> Screen Transformation:");
//        showMatrix(coordTransform);
        AffineTransform inverse = coordTransform.createInverse();

        Point2D.Float p2 = new Point2D.Float();
        inverse.transform(p1, p2);
        return p2;
    }




	
	public int getZoomLevel() {
		return zoomLevel;	
	}
	
	
	
	public void setZoomLevel(int zoomLevel) {
	
		this.zoomLevel = zoomLevel;
	
	}
	public void setMaxZoomLevel(int maxZoomLevel){
		this.maxZoomLevel = maxZoomLevel;
	}
	public void setMinZoomLevel(int minZoomLevel){
		this.minZoomLevel = minZoomLevel;
	}
	
	
	
	public AffineTransform getCoordTransform() {
	
		return coordTransform;
	
	}
	public void setTargetComp(Component targetComponent){
		this.targetComponent = targetComponent;
	}
	
	
	public void setCoordTransform(AffineTransform coordTransform) {
	
		this.coordTransform = coordTransform;
	
	}
	public void translate(double x , double y){
		coordTransform.translate(x, y);
	}
	public void setMaximumDeplacementCoordonate(double x1 , double y1 , double x2 , double y2 , boolean centered){
    	MaximumDeplacementx1 = -x1;
        MaximumDeplacementy1 = y1;
        MaximumDeplacementx2 = -x2;
        MaximumDeplacementy2 = y2;
//        if(centered == true){
//        	MaximumDeplacementx1 -= targetComponent.getWidth()/2;
//        	MaximumDeplacementx2 += targetComponent.getWidth() /(2* getZoomLevel());
//        	MaximumDeplacementy1 -= targetComponent.getHeight() /2;
//        	MaximumDeplacementy2 += targetComponent.getHeight() /2 ;
//        }
        System.out.println("target w = " + targetComponent.getWidth());
        System.out.println(MaximumDeplacementx1 + "- " +  MaximumDeplacementy1);
    }

}